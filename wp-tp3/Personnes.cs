﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace wp_tp3
{
    [DataContract]
    class Personnes
    {
        [DataMember]
        private String promo;

        [DataMember]
        private String nom;

        [DataMember]
        private String prenom;

        [DataMember]
        private String mail;

        [DataMember]
        private String tel;

        [DataMember]
        private String couleur;
        
    }
}
