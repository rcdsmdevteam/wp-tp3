﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using wp_tp3.Resources;
using System.Runtime.Serialization.Json;
using System.IO;
using System.Runtime.Serialization;
using System.Threading.Tasks;
using System.Text;

namespace wp_tp3
{
    public partial class MainPage : PhoneApplicationPage
    {
        // Constructeur
        public MainPage()
        {
            InitializeComponent();

            WebClient wc = new WebClient();

            wc.DownloadStringAsync(new Uri("http://lopma.com/rcdsm/chatsys/interfaces/info.php"));
            wc.DownloadStringCompleted += wc_DownloadStringCompleted;
        }

        void wc_DownloadStringCompleted(object sender, DownloadStringCompletedEventArgs e)
        {
            Info infos = new Info();
            DataContractJsonSerializer jsonSerializer = new DataContractJsonSerializer(typeof(Info));
            // on transforme la chaine reçu en tableau de byte
            byte[] byteArray = Encoding.UTF8.GetBytes(e.Result);
            // on désérialise la chaine lu au travers d'un MemoryStream
            infos = (Info)jsonSerializer.ReadObject(new MemoryStream(byteArray));
        }
    }
}